//
//  ImagesServiceImpl.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 29/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class ImagesServiceImpl: ImagesService {
	
	let images: Array<String>
	var lastIndexImage = 0
	
	init() {
		guard let urlResource = Bundle.main.url(forResource: "images", withExtension: "plist"),
			let imagesFromPlist = NSArray(contentsOf: urlResource) as? [String] else  {
			fatalError()
		}
		images = imagesFromPlist
	}
	
	func fetchRandomImageUrlString() -> String {
		if lastIndexImage == images.count {
			lastIndexImage = 0
		}
		let imageString = images[lastIndexImage]
		lastIndexImage += 1
		return imageString
	}
}
