//
//  ImagesService.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 24/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ImagesService {
	func fetchRandomImageUrlString() -> String
}
