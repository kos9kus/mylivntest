//
//  MainViewInput.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 29/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol MainViewInput: class {
	func setImageUrl(imageUrl: String)
}
