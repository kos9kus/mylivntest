//
//  ViewController.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, MainViewInput {
	
	@IBOutlet weak var mainImageView: UIImageView!
	var presenter: MainPresenter!

	@IBOutlet weak var spinner: SpinnerView!
	
	override func viewDidLoad() {
        super.viewDidLoad()
		mainImageView.clipsToBounds = true
		mainImageView.layer.cornerRadius = mainImageView.bounds.size.height / 2
		spinner.startRotatingAnimation()
		self.enableSpinner(enable: false)
    }
	
	func enableSpinner(enable: Bool) {
		spinner.isHidden = !enable
	}

	@IBAction func didTapLoadButton(_ sender: UIButton) {
		self.enableSpinner(enable: true)
		presenter.actionLoadImage()
	}
	
	// MARK: MainViewInput
	
	func setImageUrl(imageUrl: String) {
		mainImageView.imageLoader.image(url: imageUrl, placeholder: UIImage(named: "imagePlaceholder")!) { [weak self] in
			self?.enableSpinner(enable: false)
		}
	}
	
}

