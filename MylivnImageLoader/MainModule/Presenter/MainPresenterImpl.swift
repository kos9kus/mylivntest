//
//  MainPresenterImpl.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 29/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class MainPresenterImpl: MainPresenter {
	
	let service: ImagesService
	unowned let view: MainViewInput
	
	init(service: ImagesService, view: MainViewInput) {
		self.service = service
		self.view = view
	}
	
	func actionLoadImage() {
		let imageUrlString = service.fetchRandomImageUrlString()
		view.setImageUrl(imageUrl: imageUrlString)
	}
	
}
