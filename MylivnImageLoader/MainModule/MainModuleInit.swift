//
//  MainModuleInit.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 29/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

class MainModuleInit: NSObject {
	
	@IBOutlet weak var view: MainViewController!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		let service = ImagesServiceImpl()
		let presenter = MainPresenterImpl(service: service, view: view)
		view.presenter = presenter
	}
}
