//
//  Parser.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol Parser {
    func parse(data: Data) throws -> UIImage
}


