//
//  ImageView+ImageLoader.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

private var kImageLoaderAssociationKey: NSObject!

extension UIImageView {
    var imageLoader: MylivnImageLoader {
        get {
            if let value = objc_getAssociatedObject(self, &kImageLoaderAssociationKey) as? MylivnImageLoader {
                return value
            } else {
                let value = ImageLoaderAssembly.assembly(imageView: self)
                objc_setAssociatedObject(self, &kImageLoaderAssociationKey, value, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
                return value
            }
        }
    }
}
