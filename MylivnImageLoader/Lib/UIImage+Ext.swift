//
//  UIImage+Ext.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 29/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

extension UIImage {
	
//	func maskWithRoundForSize(size: CGSize) {
//		UIGraphicsBeginImageContextWithOptions(size, false, 0);
//		let context = UIGraphicsGetCurrentContext()!;
//		let roundedRectBounds = CGRect(origin: CGPoint.zero, size: size)//(CGRect){CGPointZero, maskSize};
//		
//		// KZ: drawing image in rounded corners
//		// UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:roundedRectBounds
//		// cornerRadius:kAvatarBezierPathCornerRadius];
//		
//		//	CGContextAddPath(context, roundedRect.CGPath);
//		CGContextAddEllipseInRect(context, roundedRectBounds);
//		[[UIColor blackColor] setFill];
//		
//		CGContextEOFillPath(context);
//		
//		UIImage *maskWithRound = UIGraphicsGetImageFromCurrentImageContext();
//		
//		UIGraphicsEndImageContext();
//		return maskWithRound;
//		
//	}
	
//	func crop() {
//		UIImage *mask = [self maskWithRoundForSize:self.size];
//		const CGSize targetSize = mask.size;
//		CGContextRef mainViewContentContext;
//		CGColorSpaceRef colorSpace;
//
//		colorSpace = CGColorSpaceCreateDeviceRGB();
//
//		// create a bitmap graphics context the size of the image
//		mainViewContentContext = CGBitmapContextCreate(NULL, targetSize.width, targetSize.height, 8, 0, colorSpace, kCGImageAlphaPremultipliedLast);
//
//		// free the rgb colorspace
//		CGColorSpaceRelease(colorSpace);
//
//		if (mainViewContentContext == nil)
//		{
//			return nil;
//		}
//
//		CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
//		rect.origin.x = (targetSize.width - self.size.width) / 2;
//		rect.origin.y = (targetSize.height - self.size.height) / 2;
//		CGImageRef maskImage = [mask CGImage];
//		CGContextClipToMask(mainViewContentContext, CGRectMake(0, 0, targetSize.width, targetSize.height), maskImage);
//		CGContextDrawImage(mainViewContentContext, rect, self.CGImage);
//
//		// Create CGImageRef of the main view bitmap content, and then
//		// release that bitmap context
//		CGImageRef mainViewContentBitmapContext = CGBitmapContextCreateImage(mainViewContentContext);
//		CGContextRelease(mainViewContentContext);
//
//		// convert the finished resized image to a UIImage
//		UIImage *theImage = [UIImage imageWithCGImage:mainViewContentBitmapContext];
//		// image is retained by the property setting above, so we can
//		// release the original
//		CGImageRelease(mainViewContentBitmapContext);
//
//		// return the image
//		return theImage;
//	}
}
