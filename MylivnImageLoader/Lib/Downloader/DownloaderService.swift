//
//  DownloaderService.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

protocol DownloaderService {
    func cancel()
    func loadImage(url: String, completion: @escaping (UIImage) -> ())
}




