//
//  DownloaderServiceImpl.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class DownloaderServiceImpl: DownloaderService {
    
    typealias NetworkProviderObject = NetworkProviderImpl
    
    private let parser: Parser
    private let cache: ImageLoaderCache
    private let network: Network<NetworkProviderObject>
    
    private init(parser: Parser, cache: ImageLoaderCache, network: Network<NetworkProviderObject>) {
        self.parser = parser
        self.cache = cache
        self.network = network
    }
    
    static func build() -> DownloaderService {
        let networkProvider = NetworkProviderImpl()
        let network = Network(networkProvider: networkProvider)
        let parser = ParserImpl()
        let cacheSettings = ImageLoaderCacheImpl.ImageLoaderCacheSettings(numberOfItems: 50, diskCacheSize: 150 * 1024 * 1024)
        let cache = ImageLoaderCacheImpl(settings: cacheSettings)
        return DownloaderServiceImpl(parser: parser, cache: cache, network: network)
    }
    
    func cancel() {
        self.network.cancel()
    }
    
    func loadImage(url: String, completion: @escaping (UIImage) -> ()) {
        guard let url = URL(string: url) else {
            return
        }
        if let image = self.cache.loadImage(name: url.lastPathComponent) {
            print("[Cache] loadImage url:\(url.lastPathComponent)")
            completion(image)
        } else {
            print("[Remote] loadImage url:\(url.lastPathComponent)")
            self.makeRequest(url: url, completion: completion)
        }
    }
    
    func makeRequest(url: URL, completion: @escaping (UIImage) -> ()) {
        
        let request = URLRequest(url: url)
        self.network.makeRequest(request: request, completion: { [weak self] (data) in
            guard let `self` = self else {
                return
            }
            do {
                let image = try self.parser.parse(data: data)
                try self.cache.setImage(image: image, name: url.lastPathComponent)
                completion(image)
            } catch let exp {
                print(exp.localizedDescription)
            }
            }, failure: { (error) in
                print(error.localizedDescription)
        })
    }
}
