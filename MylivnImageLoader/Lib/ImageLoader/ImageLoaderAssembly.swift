//
//  ImageLoaderAssembly.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

struct ImageLoaderAssembly {
    static func assembly(imageView: UIImageView) -> MylivnImageLoader {
        let downloaderService = DownloaderServiceImpl.build()
        return ImageLoader(imageView: imageView, downloader: downloaderService)
    }
}
