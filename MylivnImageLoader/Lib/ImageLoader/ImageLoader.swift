//
//  ImageLoader.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

final class ImageLoader: MylivnImageLoader {
    
    private unowned let imageView: UIImageView
    private let downloader: DownloaderService
    
    init(imageView: UIImageView, downloader: DownloaderService) {
        self.imageView = imageView
        self.downloader = downloader
    }
    
    deinit {
        downloader.cancel()
    }
    
    func image(url: String, placeholder: UIImage, completion: @escaping () -> ()) {
        imageView.image = placeholder
        downloader.cancel()
        downloader.loadImage(url: url) { [weak self] (image) in
            DispatchQueue.main.async {
                self?.imageView.image = image
                completion()
            }
        }
    }
    
    
}
