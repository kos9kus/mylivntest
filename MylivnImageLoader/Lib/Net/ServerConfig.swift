//
//  ServerConfig.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

protocol ServerConfig {
    var serverDomain: String { get }
    var scheme: String { get }
    var clientId: String { get }
    var method: String { get }
    var query: String { get }
}

extension NSURLComponents {
    static func request(serverConfig: ServerConfig) -> NSURLComponents {
        let components = NSURLComponents()
        components.scheme = serverConfig.scheme
        components.host = serverConfig.serverDomain
        components.path = serverConfig.method
        components.query = serverConfig.query
        return components
    }
}
