//
//  ServerConfigImpl.swift
//  MylivnImageLoader
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import Foundation

struct ServerConfigImpl: ServerConfig {
    var clientId: String {
        return "f1ce6aad38c81fce0f445778e66f9b4d88d6db04897202c451a1e6fc214c6540"
    }
    
    var method: String {
        return "/photos/random"
    }
    
    var query: String {
        return "client_id=" + clientId
    }
    
    var serverDomain: String {
        return "api.unsplash.com"
    }
    
    var scheme: String {
        return "https"
    }
}
