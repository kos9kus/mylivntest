//
//  SpinnerView.swift
//  MylivnImageLoader
//
//  Created by k.kusainov on 29/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import UIKit

class SpinnerView: UIView {
	
	var shapeLayer: CAShapeLayer {
		return self.layer as! CAShapeLayer
	}
	
	override class var layerClass: AnyClass {
		return CAShapeLayer.self
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setup()
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setup()
	}
	
	func setup() {
		self.shapeLayer.strokeStart = 0
		self.shapeLayer.strokeEnd = 0.1
		self.shapeLayer.borderWidth = 6
		self.shapeLayer.lineWidth = 6
		self.shapeLayer.fillColor = UIColor.clear.cgColor
		self.shapeLayer.borderColor = UIColor.red.withAlphaComponent(0.4).cgColor
		self.shapeLayer.strokeColor = UIColor.red.cgColor
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		self.shapeLayer.cornerRadius = self.bounds.size.width / 2
		self.shapeLayer.path = self.layoutPath()
	}
	
	func layoutPath() -> CGPath {
		let startOffset = -1 * Double.pi / 2
		let startAngle = -1 * 2 * Double.pi + startOffset
		let endAngle = 2 * 2 * Double.pi + startOffset
		let radius = CGFloat(ceilf(Float(self.bounds.size.height / 2))) - CGFloat(self.shapeLayer.borderWidth / 2)
		let center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
		let path = UIBezierPath.init(arcCenter: center, radius: radius, startAngle: CGFloat(startAngle), endAngle: CGFloat(endAngle), clockwise: true)
		return path.cgPath
	}
	
	func startRotatingAnimation() {
		let animation = CABasicAnimation(keyPath: "transform.rotation")
		animation.duration = 0.25 * 4
		animation.fromValue = 0
		animation.toValue = Double.pi * 2
		animation.repeatCount = Float(INT_MAX)
		animation.isRemovedOnCompletion = false
		animation.fillMode = CAMediaTimingFillMode.forwards
		self.shapeLayer.add(animation, forKey: "SpinnerViewKK")
	}
	
}
