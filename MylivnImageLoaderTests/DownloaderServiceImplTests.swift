//
//  DownloaderServiceImplTests.swift
//  MylivnImageLoaderTests
//
//  Created by KONSTANTIN KUSAINOV on 24/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import MylivnImageLoader

class DownloaderServiceImplTests: XCTestCase {
    func testLoadImage() {
        let service: DownloaderService = DownloaderServiceImpl.build()
        
        let exp = self.expectation(description: "testLoadImage")
        let textUrl = "https://images.unsplash.com/photo-1417026792760-4002c893cfa0?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjQ3NzMzfQ"
        service.loadImage(url: textUrl) { (image) in
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 3)
    }

}
