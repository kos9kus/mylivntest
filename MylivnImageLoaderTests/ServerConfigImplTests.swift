//
//  ServerConfigImplTests.swift
//  MylivnImageLoaderTests
//
//  Created by KONSTANTIN KUSAINOV on 23/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import MylivnImageLoader

class ServerConfigImplTests: XCTestCase {

    func testServerConfig() {
        let config = ServerConfigImpl()
        let components = NSURLComponents.request(serverConfig: config)
        let url = components.url?.absoluteString
        let expUrl = "https://api.unsplash.com/photos/random?client_id=f1ce6aad38c81fce0f445778e66f9b4d88d6db04897202c451a1e6fc214c6540"
        XCTAssertTrue(expUrl == url)
    }

}
