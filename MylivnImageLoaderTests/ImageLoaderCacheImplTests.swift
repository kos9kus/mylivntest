
//
//  ImageLoaderCacheImplTests.swift
//  MylivnImageLoaderTests
//
//  Created by KONSTANTIN KUSAINOV on 22/12/2018.
//  Copyright © 2018 KONSTANTIN KUSAINOV. All rights reserved.
//

import XCTest
@testable import MylivnImageLoader

class ImageLoaderCacheImplTests: XCTestCase {

    func testSetImage() {
        let settings = ImageLoaderCacheImpl.ImageLoaderCacheSettings(numberOfItems: 5, diskCacheSize: 10)
        let cache = ImageLoaderCacheImpl(settings: settings)
        let image = UIImage(named: "imagePlaceholder")!
        do {
            try cache.setImage(image: image, name: "test1")
            try cache.setImage(image: image, name: "test2")
            try cache.setImage(image: image, name: "test3")
            
            let fileManager = FileManager.default
            let files = try fileManager.contentsOfDirectory(atPath: cache.path)
            XCTAssertTrue(files.count == 1)
        } catch let exp {
            print(exp)
            XCTFail("cache failed")
        }

    }

}
